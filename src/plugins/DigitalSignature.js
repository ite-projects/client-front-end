const crypto = require('crypto');

function sign(private_key, message) {

    const signer = crypto.createSign('RSA-SHA256');
    signer.write(message);
    signer.end();

    const signature = signer.sign(private_key, 'base64')
    console.log('Digital Signature: ', signature);
    console.log('Digital Signature END:');

    return signature;
}

function verify(public_key_client, message, signature) {

    const verifier = crypto.createVerify('RSA-SHA256');
    verifier.write(message);
    verifier.end();

    const result = verifier.verify(public_key_client, signature, 'base64');
    console.log('Digital Signature Verification : ' + result);
    return result;
}

module.exports = {
    sign,
    verify
}