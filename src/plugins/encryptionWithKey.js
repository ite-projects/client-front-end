const crypto = require('crypto');

// const aesWrapper = {};

// get list of supportable encryption algorithms
getAlgorithmList = () => {
    console.log(crypto.getCiphers());
};

generateKey = () => {
    return crypto.randomBytes(32);
};

generateIv = () => {
    return crypto.randomBytes(16);
};

// separate initialization vector from message
separateVectorFromData = (data) =>  {
    console.log(data);
    console.log('data');
    var iv = data.slice(-24);
    var message = data.substring(0, data.length - 24)

    return{
        iv: iv,
        message: message
    };
}

encrypt = (key, iv, text) => {
    let encrypted = '';
    let cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    encrypted += cipher.update(Buffer.from(text), 'utf8', 'base64');
    encrypted += cipher.final('base64');

    return encrypted;
};

decrypt = (key, text) => {
    let dec = '';
    let data = separateVectorFromData(text);
    let cipher = crypto.createDecipheriv('aes-256-cbc', key,  Buffer.from(data.iv, 'base64'));
	dec += cipher.update(Buffer.from(data.message, 'base64'), 'base64', 'utf8');

	try
    {
        dec += cipher.final('utf8');
    }catch(e){
        console.log(`Bad Decrypt with key: ${key}`)
        return null
    }
    return dec;
};

// add initialization vector to message
addIvToBody = (iv, encryptedBase64) => {
    encryptedBase64 += iv.toString('base64');
    return encryptedBase64;
};

encryptText = (aesKey, message) => {
    let aesIv = generateIv();
    let encryptedMessage = encrypt(aesKey, aesIv, message);
    encryptedMessage = addIvToBody(aesIv, encryptedMessage);
    return encryptedMessage;
};

module.exports = {
    generateKey,
    encryptText,
    decrypt,
};