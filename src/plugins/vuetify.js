/* eslint-disable */
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
Vue.use(Vuetify)

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: '#00509d',
				secondary: '#fdc500',
				accent: '#001c37',
				info: '#9dcfff',
			}
		}
	}
})
