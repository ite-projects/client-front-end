const state = {}
const getters = {}
const mutations = {}
const actions = {
	// register request
	register: ({ rootState, rootGetters }, data) => {
		return new Promise((resolve, reject) => {
			// register url
			const url = rootState.API_URL + 'auth/register'

			axios({
				...rootGetters.axiosConfig,
				method: 'post',
				url: url,
				data
			})
				.then(response => {
					resolve(response)
				})
				.catch(error => {
					reject(error)
				})
		})
	},

	// login request
	login: ({ rootState, rootGetters }, data) => {
		return new Promise((resolve, reject) => {
			// login url
			const url = rootState.API_URL + 'auth/login'

			axios({
				...rootGetters.axiosConfig,
				method: 'post',
				url: url,
				data
			})
				.then(response => {
					resolve(response)
				})
				.catch(error => {
					reject(error)
				})
		})
	}
}

export default {
	state,
	getters,
	mutations,
	actions
}
