const state = {
	API_URL: process.env.VUE_APP_API_URL,
}
const getters = {
	Authorization: (state, getters, rootState, rootGetters) => {
		return rootGetters.TOKEN ? 'Bearer ' + rootGetters.TOKEN : null
	},
	axiosConfig: (state, getters) => {
		return {
			API_URL: state.API_URL,
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'Authorization': getters.Authorization
			}
		}
	}
}

const mutations = {}
const actions = {}

export default {
	state,
	getters,
	mutations,
	actions
}
