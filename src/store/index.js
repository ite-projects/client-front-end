import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import axiosConfig from './axios'
import auth from './auth'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		enc_type: 'none',
		enc_key: ''
	},
	mutations: {
		update_enc_type: (state, value) => {
			state.enc_type = value
			console.log('value vuex', value)
		}
	},
	actions: {
		server_connect: ({ rootGetters }, data) => {
			return new Promise((resolve, reject) => {
				// login url
				const url = rootState.API_URL + 'auth/connect'

				axios({
					...rootGetters.axiosConfig,
					method: 'post',
					url: url,
					data
				})
					.then(response => {
						resolve(response)
					})
					.catch(error => {
						reject(error)
					})
			})
		}
	},
	modules: {
		axiosConfig,
		auth,
	}
})
